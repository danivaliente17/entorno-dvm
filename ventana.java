package ventana;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JLabel;

/**
 * 
 * @author danielvaliente
 *@param ventana 
 *@return 
 */


public class ventana extends JFrame {
	public JPanel panel; 
 /**
  * Metodo de creacion de ventana
  */
	public ventana() { //Constructor 
		this.setSize(600,700);//Tamaño de la ventena 
		setDefaultCloseOperation(EXIT_ON_CLOSE);//Cerrar automáticamente el programa cuando se cierre la ventana
		setTitle("CALCULADORA-DVM");//Titulo de nuestra ventana
		setLocation(500,200);//Colocación de nuestra ventana en la pantalla
		iniciarComponentes();//Llamamos al método
		this.getContentPane().setBackground(Color.yellow);//Color de nuestra ventana
	}
	/**
	  * metodo de inicar los componentes de nuestra calculadora 
	  */
	private void iniciarComponentes() { //private porque no quiero que nadie tenga acceso  este metodo y sea un metodo exclusivo 
		colocarPaneles();
		colocarEtiquetas();
		colocarBotones();
	}
	/**
	  * metodo para la colocacion de las etiquetas
	  */
	private void colocarEtiquetas(){
		panel.setBackground(Color.pink);//Color del panel 
		JLabel etiqueta = new JLabel("Calculadora-DVM");//Creacion de mnuestra etiqueta
		panel.add(etiqueta);//Agregamos la etiqueta al panel 
	}
	/**
	  *metodo para la colocacion de panales  
	  */
	private void colocarPaneles() {
		panel = new JPanel();//Creacion del panel 
		this.getContentPane().add(panel);//Agregamos el panel a la ventana
		panel.setLayout(null); //Deshabilitamos el diseño del panel 
	}
	/**
	  * metodo para la colocacion de botones 
	  */
	private void colocarBotones() {
		JButton boton1 = new JButton("0"); //Creacion del boton 
		boton1.setBounds(200,600,70,40); // Tamaño y poscion de nuestro boton 
		boton1.setForeground(Color.blue);//Color  del numero dentro del boton 
		panel.add(boton1); //Agregamos nuestro boton al panel 
		
		JButton boton2 = new JButton("1");
		boton2.setBounds(60,530,70,40);
		boton2.setForeground(Color.blue);
		panel.add(boton2);
		
		JButton boton3 = new JButton("2");
		boton3.setBounds(200,530,70,40);
		boton3.setForeground(Color.blue);
		panel.add(boton3);
		
		JButton boton4 = new JButton("3");
		boton4.setBounds(340,530,70,40);
		boton4.setForeground(Color.blue);
		panel.add(boton4);
		
		JButton boton5 = new JButton("4");
		boton5.setBounds(60,470,70,40);
		boton5.setForeground(Color.blue);
		panel.add(boton5);
		
		JButton boton6 = new JButton("5");
		boton6.setBounds(200,470,70,40);
		boton6.setForeground(Color.blue);
		panel.add(boton6);
		
		JButton boton7 = new JButton("6");
		boton7.setBounds(340,470,70,40);
		boton7.setForeground(Color.blue);
		panel.add(boton7);
		
		JButton boton8 = new JButton("7");
		boton8.setBounds(60,410,70,40);
		boton8.setForeground(Color.blue);
		panel.add(boton8);
		
		JButton boton9 = new JButton("8");
		boton9.setBounds(200,410,70,40);
		boton9.setForeground(Color.blue);
		panel.add(boton9);
		
		JButton boton10 = new JButton("9");
		boton10.setBounds(340,410,70,40);
		boton10.setForeground(Color.blue);
		panel.add(boton10);
		
		JButton boton11 = new JButton("+");
		boton11.setBounds(450,600,70,40);
		boton11.setForeground(Color.red);
		panel.add(boton11);
		
		JButton boton12 = new JButton("-");
		boton12.setBounds(450,530,70,40);
		boton12.setForeground(Color.red);
		panel.add(boton12);
		
		JButton boton13 = new JButton("*");
		boton13.setBounds(450,460,70,40);
		boton13.setForeground(Color.red);
		panel.add(boton13);
		
		JButton boton14 = new JButton("/");
		boton14.setBounds(450,390,70,40);
		boton14.setForeground(Color.red);
		panel.add(boton14);
	}
	/**
	  * metodo para la suma 
	  * @param a
	  * @param b 
	  * @return
	  */
	public static int suma(int a, int b) {
		
		return a+b;
	}
	/**
	  * metodo para la resta 
	  * @param a
	  * @param b 
	  * @return
	  */
	public static int resta(int a, int b) {
		
		return a-b;
	}
	/**
	  * metodo para la division
	  * @param a
	  * @param b 
	  * @return 
	  */
	public static int multiplicacion(int a, int b) {
		
		return a*b;
	}
	/**
	  * metodo para la multiplicacion 
	  * @param a
	  * @param b 
	  * @return
	  */
	public static int division(int a, int b) {
		
		return a/b;
	}
	
}
