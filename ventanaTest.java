package ventana;

import static org.junit.Assert.*;

import org.junit.Test;

public class ventanaTest {

	@Test
	public void testSuma() {
		int resultado=ventana.suma(2,3);
		int esperado =5; //2+3=5
		assertEquals(esperado,resultado);
	}
	
	@Test
	public void testResta() {
		int resultado=ventana.resta(3,2);
		int esperado =1; //3-2=1
		assertEquals(esperado,resultado);
	}
	
	@Test
	public void testMultiplicaion() {
		int resultado=ventana.multiplicacion(2,3);
		int esperado =6;//2*3=6
		assertEquals(esperado,resultado);
	}
	
	@Test
	public void testDivision() {
		int resultado=ventana.division(10,5);
		int esperado =2;//10/5=2
		assertEquals(esperado,resultado);
	}

}
